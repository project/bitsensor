<?php

namespace Drupal\bitsensor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BitsensorForm.
 *
 * @package Drupal\bitsensor\Form
 */
class BitsensorForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bitsensor.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitsensor_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bitsensor.settings');

    $form['bitsensor_io_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URI'),
      '#description' => $this->t('The API URI to the service.'),
      '#default_value' => $config->get('bitsensor_io_uri'),
    );

    $form['bitsensor_io_user'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#description' => $this->t('The user to connect to the service.'),
      '#default_value' => $config->get('bitsensor_io_user'),
    );

    $form['bitsensor_io_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The API key to connect to the service'),
      '#default_value' => $config->get('bitsensor_io_api_key'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('bitsensor.settings')
      ->set('bitsensor_io_uri', $form_state->getValue('bitsensor_io_uri'))
      ->set('bitsensor_io_user', $form_state->getValue('bitsensor_io_user'))
      ->set('bitsensor_io_api_key', $form_state->getValue('bitsensor_io_api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
