<?php

namespace Drupal\bitsensor\Listeners;

use BitSensor\Core\BitSensor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use BitSensor\Connector\ApiConnector;
use BitSensor\Blocking\Blocking;
use BitSensor\Blocking\Action\BlockingpageAction;
use BitSensor\Handler\IpHandler;
use BitSensor\Handler\AfterRequestHandler;


/**
 * Class BitsensorListener.
 *
 * @package Drupal\bitsensor\Listeners
 */
class BitsensorListener implements EventSubscriberInterface {

  /**
   * Bitsensor request listener.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Event we are listening to.
   */
  public function bitSensor(GetResponseEvent $event) {
    $config = \Drupal::config('bitsensor.settings');
    ApiConnector::setUser($config->get('bitsensor_io_user'));
    ApiConnector::setApiKey($config->get('bitsensor_io_api_key'));
    BlockingpageAction::setUser($config->get('bitsensor_io_user'));
    BlockingpageAction::setHost($config->get('bitsensor_io_uri'));
    Blocking::setAction(new BlockingpageAction());
    BitSensor::setConnector(new ApiConnector());
    IpHandler::setIpAddressSrc(IpHandler::IP_ADDRESS_REMOTE_ADDR);
    // If you are using FastCGI.
    AfterRequestHandler::setExecuteFastcgiFinishRequest(TRUE);
    // If you have enabled UOPZ.
    BitSensor::setEnbaleUopzHook(TRUE);

    // Start BitSensor.
    BitSensor::run();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST] = array('bitSensor');
    return $events;
  }

}
